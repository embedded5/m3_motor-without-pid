/* 
 * File:   debug.h
 * Author: ilyapozdneev
 *
 * Created on February 5, 2017, 8:56 PM
 */

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include "system_config.h"
#include "system_definitions.h"

/*
 adding small changes just to test git
 * 
 * another change for tutorial 
 */

#ifndef DEBUG_H
#define	DEBUG_H

#define LOC_INIT 0xAA
#define LOC_ENTERED_TASK 0xFF
#define LOC_LOOP 0x01

#define LOC_ISR 0x10
#define LOC_Tx_ISR 0x11
#define LOC_Rx_ISR 0x12
#define LOC_Er_ISR 0x13

#define LOC_BEFORE_SEND_Q 0xB3
#define LOC_AFTER_SEND_Q 0xB4
#define LOC_BEFORE_RECEIVE_Q 0xB3
#define LOC_AFTER_RECEIVE_Q 0xB4

#define LOC_BEFORE_SEND_ISRQ 0xA1
#define LOC_AFTER_SEND_ISRQ 0xA2
#define LOC_BEFORE_RECEIVE_ISRQ 0xA3
#define LOC_AFTER_RECEIVE_ISRQ 0xA4

#define LOC_ENTER_ISR 0xC1
#define LOC_LEAVE_ISR 0xC2

void dbgOutputVal(unsigned char outVal);

void dbgUARTVal(unsigned char outVal);

void dbgOutputLoc(unsigned char outVal);


#endif	/* DEBUG_H */

