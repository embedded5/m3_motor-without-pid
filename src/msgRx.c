#include "msgrx.h"
#include "msgRx_pub.h"
#include "msgTx_pub.h"
#include "motorTask_pub.h"
#include "jsmn.h"
//#include "motor.h"
#include "messageID.h"
//#include "jWrite.h"

#define START '@'
#define END '#'
#define GET_STATS '~'
//#define PAUSE '!'

#if DO_DEBUG
#include "debug.h"
#endif

/* Comm Stats Stuff */
void postCommStats();
void sendInitSequence();
void sendStartCommand();
//void sendOpen();
//void sendCommand();

/* Motor Controls stuff */
void requestMotorStats();
void sendMotorCommand(char m,int val); // used to set motor speed, mostly during debugging


void processMotorCommand(const char *json,int r, int j);
void processRequest(const char *json, int r, int j);

void sendDummyMap(int sNum);
void requestPosition(int sNum);


int received = 0;
//int numTx = 0;
int parsed = 0;
int bad = 0;
int garbageBytes = 0;
/* end of that stuff */

QueueHandle_t xQueueRx;
enum rx_state_t state = EMPTY;
char rx_string[600];
jsmn_parser p;
jsmntok_t t[100];

// msg buffer setup
#define MSG_BUFFER_HEIGHT 20
#define MSG_BUFFER_WIDTH 2
int msgBuffer[MSG_BUFFER_HEIGHT][MSG_BUFFER_WIDTH];
// end buffer setup

static int jsoneq(const char *json, jsmntok_t *tok, const char *s) {
        if (tok->type == JSMN_STRING && (int) strlen(s) == tok->end - tok->start &&
                strncmp(json + tok->start, s, tok->end - tok->start) == 0) {
                return 1;
        }
        return 0;
}

void MSGRX_Initialize ( void )
{
#if DO_DEBUG
    dbgOutputLoc(0x00);
    dbgOutputLoc(LOC_INIT);
#endif
    xQueueRx = xQueueCreate( 600, sizeof( char ) );
    jsmn_init(&p);
    
    int i=0;
    while (i<MSG_BUFFER_HEIGHT) {
        msgBuffer[i][0] = MSG_EMPTY;
        msgBuffer[i][1] = 0;
        i += 1;
    }
}
//static const char *JSON_STRING = "{\"user\": \"johndoe\", \"admin\": false, \"uid\": 1000,\n""\"groups\": [\"users\", \"wheel\", \"audio\", \"video\"]}";

/******************************************************************************
  Function:
    void MSGRX_Tasks ( void )

  Remarks:
    See prototype in msgrx.h.
 */

void MSGRX_Tasks ( void )
{
#if DO_DEBUG
    dbgOutputLoc(LOC_ENTERED_TASK);
#endif
    
    //SYS_INT_SourceStatusClear(INT_SOURCE_USART_1_TRANSMIT);
    SYS_INT_SourceStatusClear(INT_SOURCE_USART_1_RECEIVE);
    PLIB_INT_SourceEnable(INT_ID_0,INT_SOURCE_USART_1_RECEIVE);
    //PLIB_INT_SourceEnable(INT_ID_0,INT_SOURCE_USART_1_TRANSMIT);
    
    int loc = 0;
    int i = 0;
    int r = 0;
    state = INIT_C;
    while(1) {
#if DO_DEBUG
        dbgOutputLoc(LOC_LOOP); 
#endif
        char byte;
        //dbgOutputLoc(LOC_BEFORE_RECEIVE_Q);
        //xQueueReceive( xQueueRx, ( void * ) &byte, portMAX_DELAY );
        //dbgOutputLoc(LOC_AFTER_RECEIVE_Q);
        //byte += 1;
        //app1PostToTxQ(byte);
        xQueueReceive( xQueueRx, ( void * ) &byte, portMAX_DELAY );
        switch(state) {
            case INIT_C:
                if (byte == 'C')
                    state = INIT_M;
                break;
            case INIT_M:
                //xQueueReceive( xQueueRx, ( void * ) &byte, portMAX_DELAY );
                if (byte == 'M')
                    state = INIT_D;
                break;
            case INIT_D:
                //xQueueReceive( xQueueRx, ( void * ) &byte, portMAX_DELAY );
                if (byte == 'D') {
                    sendInitSequence();
                    sendStartCommand();
                    DRV_TMR0_Start();
                    state = EMPTY;
                }
                break;
            case EMPTY:
                // <-- TODO: Add overflow proteciton, error handling -->
                //xQueueReceive( xQueueRx, ( void * ) &byte, portMAX_DELAY );
                if (byte == START) {
                    state = STARTED;
                } else if (byte == GET_STATS) {
                    postCommStats();
                    requestMotorStats();
                } 
                else {
                    garbageBytes += 1;
                    //app1PostToTxQ(byte);
                    // do something maybe
                }
                break;
            case (STARTED):
                // <-- TODO: add misplaced start error -->
                //xQueueReceive( xQueueRx, ( void * ) &byte, portMAX_DELAY );
                //app1PostToTxQ(byte);
                if (byte != END) {
                    rx_string[loc] = byte;
                    loc += 1;
                } else {
                    state = COMPLETED;
                }
                break;
            case (COMPLETED):
                // <-- TODO: better error hadling and error json generation -->
                received += 1;
                r = parseJson(loc);
                if (r < 0) {
                    app1PostToTxQ('b');
                    bad += 1;
                    if (r == -1)
                        app1PostToTxQ('1');
                    else if (r == -2)
                        app1PostToTxQ('2');
                    else if (r == -3)
                        app1PostToTxQ('3');
                } else if (r < 1 || t[0].type != JSMN_OBJECT) {
                    app1PostToTxQ('m');
                } else {
                    //app1PostToTxQ('g');
                    parsed += 1;
                    processJson(rx_string,r);
                }
                loc = 0;
                i = 0;
                state = SEND_COMM_STATS; 
                break;
            case (SEND_COMM_STATS):
                postCommStats();
                state = EMPTY;
                break;
        }
    }
}

void app1PostToRxQ_ISR(char b,BaseType_t *pxHigherPriorityTaskWoken)
{
#if DO_DEBUG
    dbgOutputLoc(LOC_BEFORE_SEND_ISRQ);
#endif
    xQueueSendFromISR( xQueueRx, &b, pxHigherPriorityTaskWoken );
#if DO_DEBUG
    dbgOutputLoc(LOC_AFTER_SEND_ISRQ);
#endif
}

void app1PostToRxQ(char b)
{
#if DO_DEBUG
    dbgOutputLoc(LOC_BEFORE_SEND_Q);
#endif
    xQueueSend( xQueueRx, &b, portMAX_DELAY );
#if DO_DEBUG
    dbgOutputLoc(LOC_AFTER_SEND_Q);
#endif
}

int parseJson(int loc) {
    return jsmn_parse(&p, rx_string, loc+1, t, sizeof(t)/sizeof(t[0]));
}

/*
 * json - full json string
 * r - total number of tokens
 */
int processJson(const char *json, int r) {
    int j = 0;
    while (j < r) {
        /* Print Everything */
        /*
        int i = t[j].start;
        while (i<t[j].end) {
            app1PostToTxQ(rx_string[i]);
            i+=1;
        }
        app1PostToTxQ('\n');
        j+=1;
        */
        if (jsoneq(json,&t[j],"motorCommand")) {
            processMotorCommand(json,r,j);
        } else if (jsoneq(json,&t[j],"request")) {
            processRequest(json,r,j);
        }  
        j+=1;
    }
}

/*
 * json - json string
 * r - total number of json tokens
 * j - current token index, so don't have to go through the whole thing again
 */

void processMotorCommand(const char *json,int r, int j) {
    while (j < r) {
        if (jsoneq(json,&t[j],"start")) {
            int turn = 0;
            int i = t[j+1].start;
            while (i<t[j+1].end) {               
                turn *= 10;
                turn += (int)(rx_string[i]-'0');               
               i+=1;
            }
            sendMotorCommand('s',turn);
        } 
        else if (jsoneq(json,&t[j],"distance")) {
            int distance = 0;
            int i = t[j+1].start;
            while (i<t[j+1].end) {               
                distance *= 10;
                distance += (int)(rx_string[i]-'0');               
               i+=1;
            }
            sendMotorCommand('d',distance);
       }
        j+=1;
    }
}

void processRequest(const char *json, int r, int j) {
    int sequenceNumber = 0;
    int k = j;
    while (k < r) {
        if (jsoneq(json,&t[k],"sequenceNumber")) {
            int i = t[k+1].start;
            while (i<t[k+1].end) {
                //app1PostToTxQ(rx_string[i]);
                sequenceNumber *= 10;
                sequenceNumber += (int)(rx_string[i]-'0');
                i+=1;
            }
        }
        k += 1;
    }
    while (j < r) {
        if (jsoneq(json,&t[j],"mapData")) {
            sendDummyMap(sequenceNumber);
        } else if (jsoneq(json,&t[j],"position")) {
            requestPosition(sequenceNumber);
        }
        j += 1;
    }
}

/* Function that sends current commStats values to the Tx Value Q */
void postCommStats()
{
    int i=0;
    bool done = false;
    // find empty slots
    int empty[4] = {-1,-1,-1,-1};
    int e = 0;
    while (i < MSG_BUFFER_HEIGHT && e < 4) {
        //postToMsgQ()
        if (msgBuffer[i][0] == MSG_EMPTY) {
            empty[e] = i;
            e += 1;
        }
        i += 1;
    }
    
    if (e == 4) {
        msgBuffer[empty[0]][0] = MSG_NUM_RECEIVED;
        msgBuffer[empty[0]][1] = received;
        postToMsgQ(msgBuffer[empty[0]]);
        
        msgBuffer[empty[1]][0] = MSG_NUM_PARSED;
        msgBuffer[empty[1]][1] = parsed;
        postToMsgQ(msgBuffer[empty[1]]);
        
        msgBuffer[empty[2]][0] = MSG_NUM_BAD;
        msgBuffer[empty[2]][1] = bad;
        postToMsgQ(msgBuffer[empty[2]]);
        
        msgBuffer[empty[3]][0] = MSG_NUM_GARBAGE_BYTES;
        msgBuffer[empty[3]][1] = garbageBytes;
        postToMsgQ(msgBuffer[empty[3]]);
    } else {
        app1PostToTxQ('e');
    }
}

void requestMotorStats() {
    int i=0;
    // find empty slots
    bool done = false;
    while (i < MSG_BUFFER_HEIGHT && done == false) {
        //postToMsgQ()
        if (msgBuffer[i][0] == MSG_EMPTY) {
            done = true;
            msgBuffer[i][0] = CMD_GET_MOTOR_STATS;
            postToMotorQ(msgBuffer[i]);
        }
        i += 1;
    }
}

void sendMotorCommand(char c,int val)
{
    int m;    
    if (c == 's') {
        m = CMD_PWM_START;
    } 
    else if (c == 'd') {
        m = CMD_DISTANCE;
    }  
    else{
        return; // handle error
    }
    
    int i=0;
    // find empty slots
    bool done = false;
    while (i < MSG_BUFFER_HEIGHT && done == false) {
        //postToMsgQ()
        if (msgBuffer[i][0] == MSG_EMPTY) {
            done = true;
            msgBuffer[i][0] = m;
            msgBuffer[i][1] = val;
            postToMotorQ(msgBuffer[i]);
        }
        i += 1;
    }
}

void sendInitSequence()
{    
    int i=0;
    // find empty slots
    bool done = false;
    while (i < MSG_BUFFER_HEIGHT && done == false) {
        //postToMsgQ()
        if (msgBuffer[i][0] == MSG_EMPTY) {
            done = true;
            msgBuffer[i][0] = CMD_SEND_INIT_SEUQNECE;
            msgBuffer[i][1] = 0;
            postToMsgQ(msgBuffer[i]);
        }
        i += 1;
    }
}

void sendStartCommand()
{
    int i=0;
    // find empty slots
    bool done = false;
    while (i < MSG_BUFFER_HEIGHT && done == false) {
        //postToMsgQ()
        if (msgBuffer[i][0] == MSG_EMPTY) {
            done = true;
            msgBuffer[i][0] = CMD_START;
            msgBuffer[i][1] = 0;
            postToMsgQ(msgBuffer[i]);
        }
        i += 1;
    }
}

void sendDummyMap(int sNum) 
{
    int i=0;
    bool done = false;
    // find empty slots
    int empty[7] = {-1,-1,-1,-1,-1,-1,-1};
    int e = 0;
    while (i < MSG_BUFFER_HEIGHT && e < 7) {
        //postToMsgQ()
        if (msgBuffer[i][0] == MSG_EMPTY) {
            empty[e] = i;
            e += 1;
        }
        i += 1;
    }
    
    if (e == 7) {
        msgBuffer[empty[0]][0] = CMD_CONFIRM_REQUEST;
        msgBuffer[empty[0]][1] = sNum;
        postToMsgQ(msgBuffer[empty[0]]);
        
        msgBuffer[empty[1]][0] = MSG_OBST_ID;
        msgBuffer[empty[1]][1] = 3;
        postToMsgQ(msgBuffer[empty[1]]);
        
        msgBuffer[empty[2]][0] = MSG_OBST_X;
        msgBuffer[empty[2]][1] = 5;
        postToMsgQ(msgBuffer[empty[2]]);
        
        msgBuffer[empty[3]][0] = MSG_OBST_Y;
        msgBuffer[empty[3]][1] = 6;
        postToMsgQ(msgBuffer[empty[3]]);
        
        msgBuffer[empty[4]][0] = MSG_OBST_W;
        msgBuffer[empty[4]][1] = 4;
        postToMsgQ(msgBuffer[empty[4]]);
        
        msgBuffer[empty[5]][0] = MSG_OBST_H;
        msgBuffer[empty[5]][1] = 3;
        postToMsgQ(msgBuffer[empty[5]]);
        
        msgBuffer[empty[6]][0] = CMD_SEND_OBST;
        msgBuffer[empty[6]][1] = 0;
        postToMsgQ(msgBuffer[empty[6]]);
    } else {
        app1PostToTxQ('e');
    }
}

void requestPosition(int sNum)
{
    }
