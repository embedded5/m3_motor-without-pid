/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */
#include "system_config.h"
#include "system_definitions.h"
#ifndef _MOTOR_H    /* Guard against multiple inclusion */
#define _MOTOR_H


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif
/*
 * JP1-3 Orange - Left motor Enable - pin 3
 * JP1-A Yellow - Right motor Enable - pin 5
 * JP2-4 Blue - Right motor direction - pin x
 * JP2-S Green - Left motor direction - pin x
 */
   
void motorInit();
    
void rightMotorWrite(int val);

void leftMotorWrite(int val);

int rightEncoderRead();
int leftEncoderRead();

    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
