#ifndef _MOTOR_TASK_PUB_H    /* Guard against multiple inclusion */
#define _MOTOR_TASK_PUB_H

/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif

    void postToMotorQ(int *b);
    
    void postToMotorQ_ISR(int ID, int VAL,BaseType_t *pxHigherPriorityTaskWoken);
    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
