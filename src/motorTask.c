#include "motortask.h"
#include "motorTask_pub.h"
#include "motor.h"
#include "messageID.h"
#include "msgTx_pub.h"

void postMotorStats();
void updatePID(int currentSpeed_l, int currentSpeed_r);

// msg buffer setup
#define MSG_BUFFER_HEIGHT 10
#define MSG_BUFFER_WIDTH 2
unsigned int motorBuffer[MSG_BUFFER_HEIGHT][MSG_BUFFER_WIDTH];
const unsigned int read_encoders[] = {CMD_READ_ENCODERS,0};
// end buffer setup

// MsgQ allocation
QueueHandle_t xQueueMotor;

//MOTOR//CONTROL//
uint32_t encoder_r = 0;
uint32_t encoder_l = 0;
uint32_t c_encoder_r = 0;
uint32_t c_encoder_l = 0;
uint32_t p_encoder_r = 0;
uint32_t p_encoder_l = 0;
uint32_t r_target = 0;
uint32_t l_target = 0;
uint32_t r_speed = 0;
uint32_t l_speed = 0;
uint32_t turn = 0;
uint32_t debug = 0;
//////TIME////////
uint32_t currentMilliseconds = 0;
uint32_t timelength = 0;
//////PID/////////
double pidTerm_r = 0;
double pidTerm_l = 0;
int error_r = 0;
int error_l = 0;
int area_error_r = 0;
int area_error_l = 0;
int last_error_r = 0;
int last_error_l = 0;
int D_error_r = 0;
int D_error_l = 0;
int Updated_PWM_l = 790;
int Updated_PWM_r = 750;
double Kp = 0.3, Ki = 0.0015, Kd = 0.5; 
////////////////////
int tape_Sensor_fl = 0;
int tape_Sensor_fr = 0;
int tape_Sensor_bl = 0;
int tape_Sensor_br = 0;

void MOTORTASK_Initialize ( void )
{
    int i=0;
    while (i<MSG_BUFFER_HEIGHT) {
        motorBuffer[i][0] = MSG_EMPTY;
        motorBuffer[i][1] = 0;
        i += 1;
    }
    xQueueMotor = xQueueCreate(10, sizeof( unsigned int* ));
    PLIB_INT_SourceEnable(INT_ID_0,INT_SOURCE_TIMER_5);
}

void MOTORTASK_Tasks ( void )
{   
    motorInit();    
    // This timer will be started by Rx task for now, to prevent sending stats before wifly is ready
    //DRV_TMR0_Start(); // PWM Timer and millisecond timer
    PLIB_INT_SourceEnable(INT_ID_0,INT_SOURCE_TIMER_2); // enable millisecond counting interrupts
    
    DRV_TMR3_Start(); // sensor read timer
    
    int *b = motorBuffer[0];
    
  


    while (1) {       
        xQueueReceive(xQueueMotor,&b,portMAX_DELAY);
        
        // check tape sensors
        //tape_Sensor_fl = PLIB_PORTS_PinGet(PORTS_MODULE_ID index, PORTS_CHANNEL channel, PORTS_BIT_POS bitPos);
        //tape_Sensor_fr = PLIB_PORTS_PinGet(PORTS_MODULE_ID index, PORTS_CHANNEL channel, PORTS_BIT_POS bitPos);
        //tape_Sensor_bl = PLIB_PORTS_PinGet(PORTS_MODULE_ID index, PORTS_CHANNEL channel, PORTS_BIT_POS bitPos);
        //tape_Sensor_br = PLIB_PORTS_PinGet(PORTS_MODULE_ID index, PORTS_CHANNEL channel, PORTS_BIT_POS bitPos);
    
        if(tape_Sensor_fl || tape_Sensor_fr || tape_Sensor_bl || tape_Sensor_br){
            leftMotorWrite(0);           
            rightMotorWrite(0);
            
            Updated_PWM_l = 790;
            Updated_PWM_r = 750;
            
            r_target = 0;
            l_target = 0;       
            
            PLIB_TMR_Counter16BitClear(TMR_ID_3);
            PLIB_TMR_Counter16BitClear(TMR_ID_4);    
        }
        // distance , clear encoder after the command end. 
        if((encoder_r>r_target) ||(encoder_l>l_target)){
            leftMotorWrite(0);           
            rightMotorWrite(0);
            
            Updated_PWM_l = 790;
            Updated_PWM_r = 750;
            
            r_target = 0;
            l_target = 0;       
            
            PLIB_TMR_Counter16BitClear(TMR_ID_3);
            PLIB_TMR_Counter16BitClear(TMR_ID_4);          
        }
       
        
        switch (b[0]) {
            case CMD_READ_ENCODERS:
                encoder_r = rightEncoderRead();
                encoder_l = leftEncoderRead();                            
                b[0] = MSG_EMPTY;
                break;
                               
            case CMD_PWM_START:  
                debug = b[1];
                if(b[1]==0){
                    PLIB_PORTS_PinWrite (PORTS_ID_0, PORT_CHANNEL_B, PORTS_BIT_POS_0, 0);
                    PLIB_PORTS_PinWrite (PORTS_ID_0, PORT_CHANNEL_B, PORTS_BIT_POS_1, 0);
                                      
                    Updated_PWM_l = Updated_PWM_l-pidTerm_l-175;
                    Updated_PWM_r = Updated_PWM_r+pidTerm_r;
                    leftMotorWrite (Updated_PWM_l);
                    rightMotorWrite(Updated_PWM_r);    
                    b[0] = MSG_EMPTY;
                    break;
                }
                else if(b[1]==1){ 
                    PLIB_PORTS_PinWrite (PORTS_ID_0, PORT_CHANNEL_B, PORTS_BIT_POS_0, 1);
                    PLIB_PORTS_PinWrite (PORTS_ID_0, PORT_CHANNEL_B, PORTS_BIT_POS_1, 0);
                    
                    Updated_PWM_l = Updated_PWM_l-pidTerm_l-175;
                    Updated_PWM_r = Updated_PWM_r+pidTerm_r;
                    leftMotorWrite (Updated_PWM_l);
                    rightMotorWrite(Updated_PWM_r);    
                    b[0] = MSG_EMPTY;
                    break;
                }
                else if(b[1]==2){ 
                    PLIB_PORTS_PinWrite (PORTS_ID_0, PORT_CHANNEL_B, PORTS_BIT_POS_0, 0);
                    PLIB_PORTS_PinWrite (PORTS_ID_0, PORT_CHANNEL_B, PORTS_BIT_POS_1, 1);
                    
                    Updated_PWM_l = Updated_PWM_l-pidTerm_l-175;
                    Updated_PWM_r = Updated_PWM_r+pidTerm_r;
                    leftMotorWrite (Updated_PWM_l);
                    rightMotorWrite(Updated_PWM_r);    
                    b[0] = MSG_EMPTY;
                    break;
                }
                else if(b[1]==3){ 
                    PLIB_PORTS_PinWrite (PORTS_ID_0, PORT_CHANNEL_B, PORTS_BIT_POS_0, 1);
                    PLIB_PORTS_PinWrite (PORTS_ID_0, PORT_CHANNEL_B, PORTS_BIT_POS_1, 1);
                    
                    Updated_PWM_l = Updated_PWM_l-pidTerm_l-175;
                    Updated_PWM_r = Updated_PWM_r+pidTerm_r;
                    leftMotorWrite (Updated_PWM_l);
                    rightMotorWrite(Updated_PWM_r);    
                    b[0] = MSG_EMPTY;
                    break;
                }
                             
            case CMD_GET_MOTOR_STATS:
                b[0] = MSG_EMPTY;
                postMotorStats();
                break; 
                               
            case CMD_DISTANCE: 
                 if(debug == 0 || debug == 3){
					r_target = b[1];
					l_target = b[1];
					b[0]=MSG_EMPTY;
					break; 
                }
                 else {
                    r_target = (b[1]-77)/10;
					l_target = (b[1]-77)/10;
					b[0]=MSG_EMPTY;
					break;    
                }
                b[0]=MSG_EMPTY;
                break;
                   
            case MSG_TIME_MS: 
               currentMilliseconds += b[1];
               if((currentMilliseconds % 1000 == 0)){
                    c_encoder_l= leftEncoderRead();
                    l_speed = (c_encoder_l-p_encoder_l) ;
                    c_encoder_r= rightEncoderRead();
                    r_speed = (c_encoder_r-p_encoder_r) ;
                    
                    updatePID(l_speed, r_speed);
                    
                    p_encoder_l= c_encoder_l;
                    p_encoder_r= c_encoder_r;
               }                        
                b[0]= MSG_EMPTY;
                break;         
        }
    }
   
}

void postToMotorQ(int *b)
{
    xQueueSend(xQueueMotor, &b, portMAX_DELAY);
}
/* Function that sends current motorStats values to the Tx Value Q */
void postMotorStats()
{
    int i=0;
    bool done = false;
    // find empty slots
    int empty[5] = {-1,-1,-1,-1,-1}; //array of empty indexes in the buffer
    int e = 0;
    while (i < MSG_BUFFER_HEIGHT && e < 5) {
        //postToMsgQ()
        if (motorBuffer[i][0] == MSG_EMPTY) {
            empty[e] = i;
            e += 1;
        }
        i += 1;
    }
    
    if (e == 5) {
        motorBuffer[empty[0]][0] = MSG_SPEED_RIGHT;
        motorBuffer[empty[0]][1] = r_target;
        postToMsgQ(motorBuffer[empty[0]]);
        
        motorBuffer[empty[1]][0] = MSG_SPEED_LEFT;
        motorBuffer[empty[1]][1] = l_target;
        postToMsgQ(motorBuffer[empty[1]]);
        
        motorBuffer[empty[2]][0] = MSG_ENCODER_RIGHT;
        motorBuffer[empty[2]][1] = encoder_r;
        postToMsgQ(motorBuffer[empty[2]]);
        
        motorBuffer[empty[3]][0] = MSG_ENCODER_LEFT;
        motorBuffer[empty[3]][1] = encoder_l;
        postToMsgQ(motorBuffer[empty[3]]);
        
        motorBuffer[empty[4]][0] = CMD_SEND_MOTOR_STATS;
        motorBuffer[empty[4]][1] = 1;
        postToMsgQ(motorBuffer[empty[4]]);
        
    } else {
        app1PostToTxQ('e');
    }
    
}
void postToMotorQ_ISR(int ID, int VAL,BaseType_t *pxHigherPriorityTaskWoken) 
{
    int i = 0;
    while (i < MSG_BUFFER_HEIGHT) {
        //postToMsgQ()
        if (motorBuffer[i][0] == MSG_EMPTY) {
            break;
        }
        i += 1;
    }
    if (i >= MSG_BUFFER_HEIGHT) {
        i = 0;
    }
    motorBuffer[i][0] = ID;
    motorBuffer[i][1] = VAL;
    int *p = &motorBuffer[i];
    xQueueSendFromISR( xQueueMotor, &p, pxHigherPriorityTaskWoken );
}
                    
void updatePID(int currentSpeed_l, int currentSpeed_r)
{
 error_r = currentSpeed_r-600;  //460
 error_l = currentSpeed_l-550;  //440
 
 area_error_r = error_r  + area_error_r;  // 10+0   
 area_error_l = error_l  + area_error_l;  // 10+0
 
 D_error_r = Kd * (error_r - last_error_r); // 1*(10 -0)
 D_error_l = Kd * (error_l - last_error_l); // 1*(10 -0)
 
 pidTerm_r = (Kp * error_r) + (Ki * area_error_r) + D_error_r; //0.45*10+0.015*10+10
 pidTerm_l = (Kp * error_l) + (Ki * area_error_l) + D_error_l; //0.45*10+0.015*10+10
 
 last_error_r = error_r; //10
 last_error_l = error_l; //10 
  
  if(pidTerm_r > 150){
     pidTerm_r = 150;
 }
 else if ( pidTerm_r <-150){
     pidTerm_r = -150;
 }
 
  if(pidTerm_l > 150){
     pidTerm_l = 150;
 }
 else if ( pidTerm_l <-150){
     pidTerm_l = -150;
 } 
}


/*******************************************************************************
 End of File
 */