/*******************************************************************************
 System Interrupts File

  File Name:
    system_interrupt.c

  Summary:
    Raw ISR definitions.

  Description:
    This file contains a definitions of the raw ISRs required to support the
    interrupt sub-system.

  Summary:
    This file contains source code for the interrupt vector functions in the
    system.

  Description:
    This file contains source code for the interrupt vector functions in the
    system.  It implements the system and part specific vector "stub" functions
    from which the individual "Tasks" functions are called for any modules
    executing interrupt-driven in the MPLAB Harmony system.

  Remarks:
    This file requires access to the systemObjects global data structure that
    contains the object handles to all MPLAB Harmony module objects executing
    interrupt-driven in the system.  These handles are passed into the individual
    module "Tasks" functions to identify the instance of the module to maintain.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2011-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <xc.h>
#include <sys/attribs.h>
#include "app.h"
//#include "msgrx.h"
#include "msgRx_pub.h"
//#include "msgtx.h"
#include "msgTx_pub.h"
#include "motorTask_pub.h"
#include "messageID.h"
#include "system_definitions.h"
#include "debug.h"

// *****************************************************************************
// *****************************************************************************
// Section: System Interrupt Vector Functions
// *****************************************************************************
// *****************************************************************************
void IntHandlerDrvUsartInstance0(void)
{
    dbgOutputLoc(LOC_ISR);
    //DRV_USART0_WriteByte('h');
    BaseType_t pxHigherPriorityTaskWoken=pdFALSE;
    
    /* Reading the transmit interrupt flag */
    if(SYS_INT_SourceStatusGet(INT_SOURCE_USART_1_TRANSMIT))
    {
        //dbgOutputLoc(LOC_Tx_ISR);
        /* Disable the interrupt, to avoid calling ISR continuously*/
        SYS_INT_SourceDisable(INT_SOURCE_USART_1_TRANSMIT);
        app1GetFromTxQ(&pxHigherPriorityTaskWoken);
        /* Clear up the interrupt flag */
        SYS_INT_SourceStatusClear(INT_SOURCE_USART_1_TRANSMIT);
        //dbgOutputLoc(LOC_ISR);
    }
    
    /* Reading the receive interrupt flag */
    if(SYS_INT_SourceStatusGet(INT_SOURCE_USART_1_RECEIVE))
    {
        //dbgOutputLoc(LOC_Rx_ISR);
        app1PostToRxQ_ISR(PLIB_USART_ReceiverByteReceive(USART_ID_1),&pxHigherPriorityTaskWoken);
        /* Clear up the interrupt flag */
        SYS_INT_SourceStatusClear(INT_SOURCE_USART_1_RECEIVE);
        //dbgOutputLoc(LOC_ISR);
    }
    
    
    /* Reading the error interrupt flag */
    if(SYS_INT_SourceStatusGet(INT_SOURCE_USART_1_ERROR))
    {
        //dbgOutputLoc(LOC_Er_ISR);
        /* This means an error has occurred */
        if(PLIB_USART_ReceiverOverrunHasOccurred(USART_ID_1))
        {
            PLIB_USART_ReceiverOverrunErrorClear(USART_ID_1);
        }

        /* Clear up the error interrupt flag */
        //SYS_INT_SourceStatusClear(INT_SOURCE_USART_1_ERROR);
    }
    
    //dbgOutputLoc(LOC_ISR+5);
    //pxHigherPriorityTaskWoken=pdTRUE;
    portEND_SWITCHING_ISR(pxHigherPriorityTaskWoken);
    //dbgOutputLoc(LOC_ISR+6);
}

/*
void IntHandlerDrvTmrInstance0(void)
{
    BaseType_t pxHigherPriorityTaskWoken=pdFALSE;
    app1PostToRxQ_ISR('a',&pxHigherPriorityTaskWoken);
    //DRV_TIMER_Clear();
    PLIB_INT_SourceFlagClear(INT_ID_0,INT_SOURCE_TIMER_5);
    portEND_SWITCHING_ISR(pxHigherPriorityTaskWoken);
}
*/ 

// this is both PWM timer and millisecond timer
void IntHandlerDrvTmrInstance0(void)
{  
    BaseType_t pxHigherPriorityTaskWoken=pdFALSE;
    PLIB_INT_SourceFlagClear(INT_ID_0,INT_SOURCE_TIMER_2);
    // post to transmit q to increment global timer
    postToTxMsgQ_ISR(MSG_TIME_MS, 1,&pxHigherPriorityTaskWoken);
    postToMotorQ_ISR(MSG_TIME_MS, 1,&pxHigherPriorityTaskWoken); 
    portEND_SWITCHING_ISR(pxHigherPriorityTaskWoken);
}
    
void IntHandlerDrvTmrInstance1(void)
{
    //BaseType_t pxHigherPriorityTaskWoken=pdFALSE;
    PLIB_INT_SourceFlagClear(INT_ID_0,INT_SOURCE_TIMER_3);
    //portEND_SWITCHING_ISR(pxHigherPriorityTaskWoken);
}
    
void IntHandlerDrvTmrInstance2(void)
{
    //BaseType_t pxHigherPriorityTaskWoken=pdFALSE;
    PLIB_INT_SourceFlagClear(INT_ID_0,INT_SOURCE_TIMER_4);
    //portEND_SWITCHING_ISR(pxHigherPriorityTaskWoken);
    
}

void IntHandlerDrvTmrInstance3(void)
{
    BaseType_t pxHigherPriorityTaskWoken=pdFALSE;
    PLIB_INT_SourceFlagClear(INT_ID_0,INT_SOURCE_TIMER_5);
    DRV_TMR3_CounterClear();
    postToMotorQ_ISR(CMD_READ_ENCODERS,0,&pxHigherPriorityTaskWoken);
    portEND_SWITCHING_ISR(pxHigherPriorityTaskWoken);
}
/*******************************************************************************
 End of File
*/

