// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "msgtx.h"
#include "msgTx_pub.h"
#include "messageID.h"
#include "jWrite.h"

#if DO_DEBUG
#include "debug.h"
#endif

#define START '@'
#define END '#'

/* Comm Stats Stuff */
void createCommStats();
void sendStart();
void sendCommand();

int numRx = 0;
int numTx = 0;
int numRequestsTx = 0;
int numRequestsConfirmed = 0;
int numRequestsRx = 0;
int numRequestsProcessed = 0;
int numParsed = 0;
int numBad = 0;
int numGarbageBytes = 0;
/* end of comm stats */

/* Motor Stats */
void createMotorStats();

int rSpeed = 0;
int lSpeed = 0;
int rEncoder = 0;
int lEncoder = 0;
/* end of motor stats */

/* Obstacle stuff */
int curId = 0;
int obs_stored = 0;
#define NUM_OBSTACLES 10
struct obstacle_t obstacles[NUM_OBSTACLES];
void createObstacle(int id);

/* Request Stuff */
void createRequest();
void createRequestConfirm();
int sNum = 0;

/* Tx buffers allocation */
QueueHandle_t xQueueTx;

char tx_string[700];
int out_len = 700;
/* end of Tx buffers */

// MsgQ allocation
QueueHandle_t xQueueMsg;

// msg buffer setup
#define TX_MSG_BUFFER_HEIGHT 10
#define TX_MSG_BUFFER_WIDTH 2
int txMsgBuffer[TX_MSG_BUFFER_HEIGHT][TX_MSG_BUFFER_WIDTH];
// end buffer setup

// global millisecond timer
long unsigned int global_time_ms = 0;

void MSGTX_Initialize ( void )
{
#if DO_DEBUG
    dbgOutputLoc(0x00);
    dbgOutputLoc(LOC_INIT);
#endif
    xQueueTx = xQueueCreate( 60, sizeof( char ) );
    xQueueMsg = xQueueCreate(10, sizeof( unsigned int* ));
}


/******************************************************************************
  Function:
    void MSGTX_Tasks ( void )

  Remarks:
    See prototype in msgtx.h.
 */

void MSGTX_Tasks ( void )
{
#if DO_DEBUG
    dbgOutputLoc(LOC_ENTERED_TASK);
#endif
    
    SYS_INT_SourceStatusClear(INT_SOURCE_USART_1_TRANSMIT);
    //SYS_INT_SourceStatusClear(INT_SOURCE_USART_1_RECEIVE);
    //PLIB_INT_SourceEnable(INT_ID_0,INT_SOURCE_USART_1_RECEIVE);
    PLIB_INT_SourceEnable(INT_ID_0,INT_SOURCE_USART_1_TRANSMIT);
    
    sendStart();
    //sendCommand();
    
    int obs_idx = 0;
    bool done = false;
    
    while(1) {
        int *b;
        xQueueReceive(xQueueMsg,&b,portMAX_DELAY);
        if (b[0] == MSG_TEST) {
            app1PostToTxQ('t');
            b[0] = MSG_EMPTY;
        }
        switch (b[0]) {
            case MSG_TEST:
                app1PostToTxQ('t');
                b[0] = MSG_EMPTY;
                break;
            case CMD_START:
                b[0] = MSG_EMPTY;
                break;
            case CMD_SEND_INIT_SEUQNECE:
                b[0] = MSG_EMPTY;
                sendCommand();
                break;
            case MSG_NUM_RECEIVED:
                b[0] = MSG_EMPTY;
                numRx = b[1];
                break;
            case MSG_NUM_PARSED:
                b[0] = MSG_EMPTY;
                numParsed = b[1];
                break;
            case MSG_NUM_BAD:
                numBad = b[1];
                b[0] = MSG_EMPTY;
                break;
            case MSG_NUM_GARBAGE_BYTES:
                numGarbageBytes = b[1];
                b[0] = MSG_EMPTY;
                break;
            case CMD_SEND_COMM_STATS:
                b[0] = MSG_EMPTY;
                createCommStats();
                break;
            case MSG_SPEED_RIGHT:
                rSpeed = b[1];
                b[0] = MSG_EMPTY;
                break;
            case MSG_SPEED_LEFT:
                lSpeed = b[1];
                b[0] = MSG_EMPTY;
                break;
            case MSG_ENCODER_RIGHT:
                rEncoder = b[1];
                b[0] = MSG_EMPTY;
                break;
            case MSG_ENCODER_LEFT:
                lEncoder = b[1];
                b[0] = MSG_EMPTY;
                break;
            case CMD_SEND_MOTOR_STATS:
                b[0] = MSG_EMPTY;
                createMotorStats();
                break;
            case MSG_TIME_MS:
                b[0] = MSG_EMPTY;
                global_time_ms += b[1];
                if (global_time_ms % 1000 == 0) {
                    createCommStats();
                }
                if (global_time_ms % 2000 == 0) {
                    createRequest();
                }
                break;
            case CMD_CONFIRM_REQUEST:
                b[0] = MSG_EMPTY;
                sNum = b[1];
                createRequestConfirm();
                break;
            case MSG_OBST_ID:
                curId = 7; //b[1];
                obs_idx = 0;
                done = false;
                while(obs_idx<NUM_OBSTACLES && done == false) {
                    if (obstacles[obs_idx].id == curId) {
                        done = true;
                    }
                    obs_idx+=1;
                }
                if (!done) {
                    obstacles[obs_stored].id = curId;
                    obs_stored += 1;
                }
                b[0] = MSG_EMPTY;
                break;
            case MSG_OBST_X:
                obs_idx = 0;
                while(obs_idx<NUM_OBSTACLES) {
                    if (obstacles[obs_idx].id == curId) {
                        obstacles[obs_idx].x = b[1];
                    }
                    obs_idx+=1;
                }
                b[0] = MSG_EMPTY;
                break;
            case MSG_OBST_Y:
                obs_idx = 0;
                while(obs_idx<NUM_OBSTACLES) {
                    if (obstacles[obs_idx].id == curId) {
                        obstacles[obs_idx].y = b[1];
                    }
                    obs_idx+=1;
                }
                b[0] = MSG_EMPTY;
                break;
            case MSG_OBST_W:
                obs_idx = 0;
                while(obs_idx<NUM_OBSTACLES) {
                    if (obstacles[obs_idx].id == curId) {
                        obstacles[obs_idx].w = b[1];
                    }
                    obs_idx+=1;
                }
                b[0] = MSG_EMPTY;
                break;
            case MSG_OBST_H:
                obs_idx = 0;
                while(obs_idx<NUM_OBSTACLES) {
                    if (obstacles[obs_idx].id == curId) {
                        obstacles[obs_idx].h = b[1];
                    }
                    obs_idx+=1;
                }
                b[0] = MSG_EMPTY;
                break;
            case CMD_SEND_OBST:
                curId = b[1];
                b[0] = MSG_EMPTY;
                createObstacle(curId);
                break;
        }
    }
}

/*
 * Post and get from queues
 */

void app1GetFromTxQ(BaseType_t *pxHigherPriorityTaskWoken)
{
#if DO_DEBUG
    dbgOutputLoc(LOC_BEFORE_RECEIVE_ISRQ);
#endif
    //xQueueSendFromISR( xQueueRx, &b, pxHigherPriorityTaskWoken );
    char cTxedChar;
    if (xQueueReceiveFromISR( xQueueTx,( void * ) &cTxedChar,pxHigherPriorityTaskWoken))
    {
#if DO_DEBUG
        dbgOutputLoc(LOC_AFTER_RECEIVE_ISRQ);
#endif
        PLIB_USART_TransmitterByteSend(USART_ID_1, cTxedChar);
        SYS_INT_SourceEnable(INT_SOURCE_USART_1_TRANSMIT);
    }
}

void app1PostToTxQ(char b)
{
#if DO_DEBUG
    dbgOutputLoc(LOC_BEFORE_RECEIVE_ISRQ);
#endif
    xQueueSend( xQueueTx, &b, portMAX_DELAY );
#if DO_DEBUG
    dbgOutputLoc(LOC_AFTER_RECEIVE_ISRQ);
#endif
    SYS_INT_SourceEnable(INT_SOURCE_USART_1_TRANSMIT);
}

void postToMsgQ(int *b)
{
    xQueueSend(xQueueMsg, &b, portMAX_DELAY);
}

void postToTxMsgQ_ISR(int ID, int VAL,BaseType_t *pxHigherPriorityTaskWoken) 
{
    int i = 0;
    while (i < TX_MSG_BUFFER_HEIGHT) {
        //postToMsgQ()
        if (txMsgBuffer[i][0] == MSG_EMPTY) {
            break;
        }
        i += 1;
    }
    if (i >= TX_MSG_BUFFER_HEIGHT) {
        i = 0;
    }
    txMsgBuffer[i][0] = ID;
    txMsgBuffer[i][1] = VAL;
    int *p = &txMsgBuffer[0];
    xQueueSendFromISR( xQueueMsg, &p, pxHigherPriorityTaskWoken );
}

/*
 * JSON Creation and Sending Routines
 *  
 */

void createCommStats() 
{
    numTx += 1;
    int err;
    jwOpen( tx_string, out_len, JW_OBJECT, JW_PRETTY );  // open root node as object
    jwObj_object("definitions");
    jwObj_object("commStats");
    jwObj_int("localTimerMilliseconds",global_time_ms);
    jwObj_int("numGoodMessagesReceived",numRx);
    jwObj_int("numCommErrors",numBad);
    jwObj_int("numJSONSuccessfullyParsed",numParsed);
    jwObj_int("numJSONSent",numTx);
    jwObj_int("numRequestsSent",numRequestsTx);
    jwObj_int("numRequestsConfirmed",numRequestsConfirmed);
    jwObj_int("numGarbageBytesReceived",numGarbageBytes);
    jwEnd();
    jwEnd();
    err = jwClose();
    //sendCommand();
    int i=0;
    app1PostToTxQ(START);
    while(tx_string[i] != '\0') {
        app1PostToTxQ(tx_string[i]);
        i+=1;
    }
    app1PostToTxQ('\n');
    app1PostToTxQ(END);
    //sendOpen();
}

void createMotorStats() 
{
    numTx += 1;
    int err;
    jwOpen( tx_string, out_len, JW_OBJECT, JW_PRETTY );  // open root node as object
    jwObj_object("definitions");
    jwObj_object("motorStats");
    jwObj_int("rightMotorSpeed",rSpeed);
    jwObj_int("leftMotorSpeed",lSpeed);
    jwObj_int("rightEncoderValue",rEncoder);
    jwObj_int("leftEncoderValue",lEncoder);
    jwEnd();
    jwEnd();
    err = jwClose();
    //sendCommand();
    int i=0;
    app1PostToTxQ(START);
    while(tx_string[i] != '\0') {
        app1PostToTxQ(tx_string[i]);
        i+=1;
    }
    app1PostToTxQ('\n');
    app1PostToTxQ(END);
    //sendOpen();
}

void createRequest()
{
    numRequestsTx += 1;
    int err;
    jwOpen( tx_string, out_len, JW_OBJECT, JW_COMPACT );
    jwObj_object("definitions");
    jwObj_string("request","mapData");
    jwObj_string("author","CMRover");
    jwObj_int("sequenceNumber",numRequestsTx - 1);
    jwEnd();
    jwEnd();
    err = jwClose();
    
    int i=0;
    // start with a delimiter
    app1PostToTxQ(START);
    while(tx_string[i] != '\0') {
        app1PostToTxQ(tx_string[i]);
        i+=1;
    }
    app1PostToTxQ('\n');
    app1PostToTxQ(END);
}

void createRequestConfirm()
{
    numRequestsConfirmed += 1;
    int err;
    jwOpen( tx_string, out_len, JW_OBJECT, JW_COMPACT );
    jwObj_object("definitions");
    jwObj_string("requestConfirm","OK");
    jwObj_string("author","CMRover");
    jwObj_int("sequenceNumber",sNum);
    jwEnd();
    jwEnd();
    err = jwClose();
    
    int i=0;
    // start with a delimiter
    app1PostToTxQ(START);
    while(tx_string[i] != '\0') {
        app1PostToTxQ(tx_string[i]);
        i+=1;
    }
    app1PostToTxQ('\n');
    app1PostToTxQ(END);
}

void createObstacle(int id)
{
    int idx = 0;
    bool done = false;
    while (idx < NUM_OBSTACLES && done == false) {
        if (obstacles[idx].id == id) {
            done = true;
        }
        idx += 1;
    }
    if (!done) {
        app1PostToTxQ('e');
        return;
    }
    numTx += 1;
    int err;
    jwOpen( tx_string, out_len, JW_OBJECT, JW_PRETTY );  // open root node as object
    jwObj_object("definitions");
    jwObj_object("obstacle");
    jwObj_int("id",id);
    jwObj_int("x",obstacles[idx-1].x);
    jwObj_int("y",obstacles[idx-1].y);
    jwObj_int("w",obstacles[idx-1].w);
    jwObj_int("h",obstacles[idx-1].h);
    jwEnd();
    jwEnd();
    err = jwClose();
    //sendCommand();
    int i=0;
    app1PostToTxQ(START);
    while(tx_string[i] != '\0') {
        app1PostToTxQ(tx_string[i]);
        i+=1;
    }
    app1PostToTxQ('\n');
    app1PostToTxQ(END);
    //sendOpen();
}

void sendStart()
{
    int i=0;
    char msg[] = "$$$";
    while (i < 3) {
        char byte = msg[i];
        i += 1;
        app1PostToTxQ(byte);
    }
}


void sendCommand() 
{
    int i=0;
    char msg[] = "open 192.168.42.1 2001\r\n";
    while (i < 24) {
        char byte = msg[i];
        i += 1;
        app1PostToTxQ(byte);
    }
}

/*******************************************************************************
 End of File
 */