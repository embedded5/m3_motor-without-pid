#ifndef _MESSAGE_ID_H    /* Guard against multiple inclusion */
#define _MESSAGE_ID_H

/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif

#define MSG_EMPTY 0
#define MSG_TEST 1
    
#define MSG_TIME_MS 2
#define CMD_START 3
    
/* Communication Stats Defines */
#define MSG_NUM_RECEIVED 100
#define MSG_NUM_PARSED 101
#define MSG_NUM_BAD 102
#define MSG_NUM_GARBAGE_BYTES 103
#define MSG_NUM_SENT 104

    
    
/* Motor Stats Defines */
#define MSG_SPEED_RIGHT 110
#define MSG_SPEED_LEFT 111
#define MSG_ENCODER_RIGHT 112
#define MSG_ENCODER_LEFT 113

//////
#define TST_ANGLE  24
#define CMD_DIRECTION 27
#define CMD_DISTANCE 28
#define CMD_STOP 29   
// #define CMD_DIRECTION_RIGHT 44 
////// 
     
/* Command Defines */
#define CMD_SEND_COMM_STATS 30
#define CMD_SEND_MOTOR_STATS 31
#define CMD_GET_MOTOR_STATS 32
#define CMD_SEND_INIT_SEUQNECE 33
#define CMD_CONFIRM_REQUEST 34
    
#define MOTOR_CMD 35    
    
#define CMD_READ_ENCODERS 40
#define CMD_PWM_RIGHT 41
#define CMD_PWM_LEFT 42

    
#define MSG_OBST_ID 45
#define MSG_OBST_X 46
#define MSG_OBST_Y 47
#define MSG_OBST_W 48
#define MSG_OBST_H 49
#define CMD_SEND_OBST 50
    
    
    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
